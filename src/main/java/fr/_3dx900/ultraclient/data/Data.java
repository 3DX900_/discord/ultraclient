package fr._3dx900.ultraclient.data;

import fr._3dx900.ultraclient.manager.MySQLManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public enum Data {

    PRESENCE_ACTIVITIES("ultraclient_presence_activities", null, "name"),
    PRESENCE_ACTIVITY_TYPE("ultraclient_presence_activities", "name", "type"),
    PRESENCE_ACTIVITY_URL("ultraclient_presence_activities", "name", "url"),

    UNKNOWN(null, null, null);

    private String tableName;
    private String referencedColumnName;
    private String columnName;

    Data(String tableName, String referencedColumnName, String columnName) {
        this.tableName = tableName;
        this.referencedColumnName = referencedColumnName;
        this.columnName = columnName;
    }

    public String getTableName() {
        return tableName;
    }

    public String getReferencedColumnName() {
        return referencedColumnName;
    }

    public String getColumnName() {
        return columnName;
    }

    public Object get(String index) {
        Connection connection = MySQLManager.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM `" + getTableName() + "` WHERE `" + getReferencedColumnName() + "` = '" + index + "'");
            ResultSet result = statement.executeQuery();
            if(result.next())
                return result.getObject(getColumnName());
        } catch (Exception e) {
            e.printStackTrace();
        }
        MySQLManager.disconnect();
        return null;
    }

    public List<Object> getAll() {
        List<Object> names = new ArrayList<>();
        try(Connection connection = MySQLManager.getConnection()) {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM `" + getTableName() + "`");
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                names.add(result.getString(getColumnName()));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        MySQLManager.disconnect();
        return names;
    }
}
