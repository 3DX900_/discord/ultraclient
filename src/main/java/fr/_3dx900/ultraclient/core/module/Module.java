package fr._3dx900.ultraclient.core.module;

public class Module {

    private String name;

    public Module(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
