package fr._3dx900.ultraclient.core.module;

import fr._3dx900.ultraclient.UltraClient;
import fr._3dx900.ultraclient.data.Data;
import net.dv8tion.jda.api.entities.Activity;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class PresenceModule extends Module {

    private static List<Activity> games = new ArrayList<>();

    public PresenceModule() {
        super("presence");

        for(Object nameAsObject : Data.PRESENCE_ACTIVITIES.getAll()) {
            String name = (String) nameAsObject;
            games.add(Activity.of(Activity.ActivityType.valueOf((String) Data.PRESENCE_ACTIVITY_TYPE.get(name)), name, (String) Data.PRESENCE_ACTIVITY_URL.get(name)));
        }
        startTask();
    }

    public static void update(Activity activity) {
        UltraClient.getAPI().getPresence().setActivity(activity);
    }

    public static void update() {
        if(games.isEmpty())
            return;
        Activity activity = games.get(new Random().nextInt(games.size()));
        if(UltraClient.getAPI().getPresence().getActivity() == activity) {
            update();
            return;
        }
        update(activity);
    }

    public static void startTask() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                update();
            }
        }, 0, TimeUnit.SECONDS.toMillis(15));
    }
}
