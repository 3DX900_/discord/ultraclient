package fr._3dx900.ultraclient;

import fr._3dx900.ultraclient.core.module.PresenceModule;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;

import javax.security.auth.login.LoginException;

public class UltraClient {

    private static JDA API;

    public static void main(String[] args) {
        try {
            API = new JDABuilder(AccountType.CLIENT)
                    .setToken(args[0])
                    .build();
        } catch (LoginException e) {
            e.printStackTrace();
        }

        new PresenceModule();
    }

    public static JDA getAPI() {
        return API;
    }
}
