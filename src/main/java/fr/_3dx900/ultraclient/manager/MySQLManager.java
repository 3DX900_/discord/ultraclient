package fr._3dx900.ultraclient.manager;

import fr._3dx900.ultraclient.UltraClient;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MySQLManager {

    protected static final String DATABASE_DRIVER = "com.mysql.jdbc.Driver";
    protected static final String DATABASE_URL = "jdbc:mysql://localhost:3306/admin";
    protected static final String USERNAME = "root";
    protected static final String PASSWORD = "" /* Q@c%hbyEx4rQB9cnAKuDiKNapXKLbA8&7Jy372o7Tt$WZRvjwC*#L#JLAYFAafBY4@rVWhP7!Y#Myr%Xf&GrdSU@QBJS!sjrWZHRPg#ubQ&YBMFxT^4T7Y2o&^p!9HKh */;
    protected static final String MAX_POOL = "250";

    private static Connection connection;
    private static Properties properties;

    public MySQLManager() {}

    public static Connection getConnection() {
        return MySQLManager.connect();
    }

    protected static Properties getProperties() {
        if(MySQLManager.properties == null) {
            MySQLManager.properties = new Properties();
            MySQLManager.properties.setProperty("user", MySQLManager.USERNAME);
            MySQLManager.properties.setProperty("password", MySQLManager.PASSWORD);
            MySQLManager.properties.setProperty("MaxPooledStatements", MySQLManager.MAX_POOL);
        }
        return MySQLManager.properties;
    }

    private static Connection connect() {
        if(MySQLManager.connection == null) {
            try {
                Class.forName(MySQLManager.DATABASE_DRIVER);
                MySQLManager.connection = DriverManager.getConnection(MySQLManager.DATABASE_URL, getProperties());
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
                UltraClient.getAPI().shutdown();
            }
        }
        return MySQLManager.connection;
    }

    public static void disconnect() {
        if(MySQLManager.connection != null) {
            try {
                MySQLManager.connection.close();
                MySQLManager.connection = null;
            } catch (SQLException e) {
                e.printStackTrace();
                UltraClient.getAPI().shutdown();
            }
        }
    }
}
